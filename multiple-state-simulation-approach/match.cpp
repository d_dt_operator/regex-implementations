



#include <stdio.h>
#include "postfix.h"
#include "nfa.h"
#include "match.h"

bool match (const std::string &regex, const std::string &input)
{
    nfa_t nfa (postfix_converter_t ().convert (regex));

    for (std::string::const_iterator it = input.begin (); it != input.end (); it++)
    {
        nfa.step (*it);
    }

    return nfa.match ();
} 
            

