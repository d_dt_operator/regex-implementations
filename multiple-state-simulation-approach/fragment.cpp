



#include <stdio.h>
#include "state.h"
#include "fragment.h"

fragment_t::fragment_t (const std::list<state_t **> &out, state_t *start)

    :out_   (out)
    ,start_ (start)
{
    if (start == NULL)
    {
        fprintf (stderr, "fragment_t::fragment_t: empty start state is not allowed\n");
        return;
    }

    if (out.empty ())
    {
        fprintf (stderr, "fragment_t::fragment_t: empty out states are not allowed\n");
        return;
    }
}

