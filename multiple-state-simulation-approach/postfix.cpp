



#include <stdio.h>
#include <list>
#include <utility>
#include "postfix.h"

std::string postfix_converter_t::convert (const std::string &regex)
{
    amounts_.push_back (std::make_pair (0, 0));
    amounts_it_ = amounts_.begin ();
    amount_of_atoms_ = 0;
    amount_of_alternations_ = 0;

    bool retval;
    std::string::const_iterator it = regex.begin ();

    for (; it != regex.end (); it++)
    {
        if      (*it == '(') retval = handle_left_paren ();
        else if (*it == ')') retval = handle_right_paren ();
        else if (*it == '|') retval = handle_alternation ();
        else if (*it == '*') retval = handle_kleenestar ();
        else /* atom */      retval = handle_atom (*it);

        if (! retval)
        {
            return std::string ();
        }
    }

    if (! handle_rest ())
    {
        return std::string ();
    }

    return result_;
}

bool postfix_converter_t::handle_left_paren ()
{
    if (amount_of_atoms_ > 1)
    {
        amount_of_atoms_--;
        result_.push_back ('.');
    }

    amounts_.push_back (std::make_pair (0, 0));
    amounts_it_->first = amount_of_atoms_;
    amounts_it_->second = amount_of_alternations_;
    amounts_it_++;
    amount_of_atoms_ = 0;
    amount_of_alternations_ = 0;
    return true;
}

bool postfix_converter_t::handle_right_paren ()
{
    if (amount_of_atoms_ == 0)
    {
        fprintf (stderr, "postfix_converter_t::handle_right_paren: error:\n");
        fprintf (stderr, "postfix_converter_t::handle_right_paren: zero amount of atoms parentheses\n");
        return false;
    }

    if (amounts_it_ == amounts_.begin ())
    {
        fprintf (stderr, "postfix_converter_t::handle_right_paren: error\n");
        fprintf (stderr, "postfix_converter_t::handle_right_paren: there is no matching left paren\n");
        return false;
    }

    if (amount_of_atoms_ > 1)
    {
        amount_of_atoms_--;
        result_.push_back ('.');
    }

    for (; amount_of_alternations_ > 0; amount_of_alternations_--)
    {
        result_.push_back ('|');
    }

    amounts_it_--;
    amounts_.pop_back ();
    amount_of_atoms_ = amounts_it_->first + 1;
    amount_of_alternations_ = amounts_it_->second;
    return true;
}

bool postfix_converter_t::handle_alternation ()
{
    if (amount_of_atoms_ == 0)
    {
        fprintf (stderr, "postfix_converter_t::handle_alternation: error\n");
        fprintf (stderr, "postfix_converter_t::handle_alternation: zero amount of arguments\n");
        return false;
    }

    if (amount_of_atoms_ > 1)
    {
        amount_of_atoms_--;
        result_.push_back ('.');
    }

    amount_of_atoms_ = 0;
    amount_of_alternations_++;
    return true;
}

bool postfix_converter_t::handle_kleenestar ()
{
    if (amount_of_atoms_ == 0)
    {
        fprintf (stderr, "postfix_converter_t::handle_kleenestar: error\n");
        fprintf (stderr, "postfix_converter_t::handle_kleenestar: zero amount of arguments\n");
        return false;
    }

    result_.push_back ('*');
    return true;
}

bool postfix_converter_t::handle_atom (const char &input)
{
    if (amount_of_atoms_ > 1)
    {
        amount_of_atoms_--;
        result_.push_back ('.');
    }

    result_.push_back (input);
    amount_of_atoms_++;
    return true;
}

bool postfix_converter_t::handle_rest ()
{
    if (amounts_it_ != amounts_.begin ())
    {
        fprintf (stderr, "postfix_converter_t::handle_rest: error\n");
        fprintf (stderr, "postfix_converter_t::handle_rest: stack not empty\n");
        return false;
    }

    if (amount_of_atoms_ > 1)
    {
        amount_of_atoms_--;
        result_.push_back ('.');
    }

    for (; amount_of_alternations_ > 0; amount_of_alternations_--)
    {
        result_.push_back ('|');
    }

    return true;
}

