



#ifndef _REGEX_COMPILER_NFA_H
#define _REGEX_COMPILER_NFA_H

#include <string>

class state_t;
class fragment_t;


/* NFA representation.
 * This representaton constructs from Regex in postfix 
 * notation. The start method returns NFA to the start
 * state. The step method performs one NFA step on the
 * input character. The match method returns true if 
 * NFA in the final state, otherwise it returns false.
 */

class nfa_t
{
    public:

        nfa_t (const std::string &postfix);
       ~nfa_t ();

        void step (const char &input);  
        void start ();  
        bool match () const;            

    private:

        bool init_nfa (const std::string &);
        void free_nfa (state_t *start);


        /* compilation helpers */

        static fragment_t *create_atom          (state_t *);
        static fragment_t *create_kleenestar    (fragment_t *);
        static fragment_t *create_concatenation (fragment_t *, fragment_t *);
        static fragment_t *create_alternation   (fragment_t *, fragment_t *);


        /* execution helpers */

        void add_state (std::list<state_t *> &, state_t *);
        

        std::list<state_t *> id_list_;
        int id_counter_;
        state_t *start_;
};

#endif  // _REGEX_COMPILER_NFA_H


