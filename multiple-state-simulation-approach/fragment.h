



#ifndef _REGEX_COMPILER_FRAGMENT_H
#define _REGEX_COMPILER_FRAGMENT_H

#include <list>

class state_t;


/* The fragment of NFA.
 * This fragments are used only for Regex to NFA compilation purpose.
 * In process of compilation for each regex operator and its
 * operands such fragment is created.
 */

class fragment_t
{
    public:
        
        fragment_t (const std::list<state_t **> &, state_t *start);

        std::list<state_t **> out_;
        state_t *start_;
};

#endif  // _REGEX_COMPILER_FRAGMENT_H




