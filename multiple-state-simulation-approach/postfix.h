



#ifndef _REGEX_COMPILER_POSTFIX_H
#define _REGEX_COMPILER_POSTFIX_H

#include <string>
#include <list>


/* Regex in classic notation to Regex in postfix notation 
 * converter. Besides interest, it's usefull for simplification
 * of NFA construction process.
 */

class postfix_converter_t
{
    public:
        
        std::string convert (const std::string &regex);

    private:

        bool handle_left_paren ();
        bool handle_right_paren ();
        bool handle_alternation ();
        bool handle_kleenestar ();
        bool handle_rest ();
        bool handle_atom (const char &);

        std::string result_;
        std::list<std::pair<unsigned, unsigned> > amounts_;
        std::list<std::pair<unsigned, unsigned> >::iterator amounts_it_;
        unsigned amount_of_atoms_;
        unsigned amount_of_alternations_;
};

#endif  // _REGEX_COMPLIER_POSTFIX_H




