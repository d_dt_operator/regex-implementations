



#include <stdio.h>
#include "postfix.h"
#include "match.h"

int main (int argc, char *argv[])
{
    if (argc != 3)
    {
        fprintf (stderr, "main: wrong number of command line arguments.\n");
        fprintf (stderr, "main: try ./%s {regex} {string}\n", argv[0]);
        return 1;
    }

    printf ("main: classic notation = %s\n", argv[1]);
    printf ("main: postfix notation = %s\n", postfix_converter_t ().convert (argv[1]).c_str ());
    printf ("main: match (%s, %s) = %d\n", argv[1], argv[2], match (argv[1], argv[2]));
    return 0;
}

