



#include <algorithm>
#include <set>
#include <stdio.h>
#include "state.h"
#include "fragment.h"
#include "nfa.h"

nfa_t::nfa_t (const std::string &postfix)

    :start_ (NULL)
{
    if (postfix.empty ())
    {
        fprintf (stderr, "nfa_t::nfa_t: empty postfix notation\n");
        return;
    }

    if (! init_nfa (postfix))
    {
        fprintf (stderr, "nfa_t::nfa_t: initialization error\n");
        return;
    }

    start ();
}

nfa_t::~nfa_t ()
{
    free_nfa (start_);
}

void nfa_t::step (const char &c)
{
    id_counter_++;
    std::list<state_t *> id_list;
    std::list<state_t *>::iterator it = id_list_.begin ();

    for (; it != id_list_.end (); it++)
    {
        if ((*it)->get_char () == c)
        {
            add_state (id_list, (*it)->out1_);
        }
    }

    id_list_.swap (id_list);
}

void nfa_t::start ()
{
    if (start_ != NULL)
    {
        id_list_.clear ();
        id_counter_ = 0;
        add_state (id_list_, start_);
    }
}

bool nfa_t::match () const
{
    std::list<state_t *>::const_iterator it = id_list_.begin ();

    for (; it != id_list_.end (); it++)
    {
        if ((*it)->get_char () == state_t::Final)
        {
            return true;
        }
    }

    return false;
}

/* compile regex in postfix notation to nfa */

bool nfa_t::init_nfa (const std::string &postfix)
{
    std::list<fragment_t *> stack;
    std::string::const_iterator it = postfix.begin ();

    for (; it != postfix.end (); it++)
    {
        if (*it == '.' && stack.size () < 2) 
        {
            fprintf (stderr, "nfa_t::init_nfa: unexpected amount of operands for '.' operation\n");
            fprintf (stderr, "nfa_t::init_nfa: amount of operands = %d\n", stack.size ());
            free_nfa ((*stack.begin ())->start_); 
            return false;
        }
        else if (*it == '|' && stack.size () < 2) 
        {
            fprintf (stderr, "nfa_t::init_nfa: unexpected amount of operands for '|' operation\n");
            fprintf (stderr, "nfa_t::init_nfa: amount of operands = %d\n", stack.size ());
            free_nfa ((*stack.begin ())->start_); 
            return false;
        }
        else if (*it == '*' && stack.size () < 1) 
        {
            fprintf (stderr, "nfa_t::init_nfa: unexpected amount of operands for '*' operation\n");
            fprintf (stderr, "nfa_t::init_nfa: amount of operands = %d\n", stack.size ());
            free_nfa ((*stack.begin ())->start_); 
            return false;
        }

        if (*it == '.')
        {
            fragment_t *op2 = stack.back (); 
            stack.pop_back ();
            fragment_t *op1 = stack.back (); 
            stack.pop_back ();
            stack.push_back (create_concatenation (op1, op2));
            delete op1;
            delete op2;
        }
        else if (*it == '|')
        {
            fragment_t *op1 = stack.back ();
            stack.pop_back ();
            fragment_t *op2 = stack.back ();
            stack.pop_back ();
            stack.push_back (create_alternation (op1, op2));
            delete op1;
            delete op2;
        }
        else if (*it == '*')
        {
            fragment_t *op = stack.back ();
            stack.pop_back ();
            stack.push_back (create_kleenestar (op));
            delete op;
        }
        else 
        {
            state_t *state = new state_t (*it, NULL, NULL);
            stack.push_back (create_atom (state));
        }
    }

    fragment_t *last = stack.back ();
    stack.pop_back ();
    
    if (! stack.empty ())
    {
        fprintf (stderr, "nfa_t::init_nfa: stack isn't empty in the end of nfa's construction\n");
        fprintf (stderr, "nfa_t::init_nfa: stack size = %d\n", stack.size ());
        free_nfa (start_);
        return false;
    }

    state_t *final = new state_t (state_t::Final, NULL, NULL);
    fragment_t *fin_frag = new fragment_t (std::list<state_t **> (1, &final->out1_), final);
    fragment_t *cmp_frag = create_concatenation (last, fin_frag);
    start_ = cmp_frag->start_;

    delete cmp_frag;
    delete fin_frag;
    delete last;
    return true;
}

void nfa_t::free_nfa (state_t *state)
{
    static std::set<state_t *> black_list;
    
    if (state != NULL && black_list.find (state) == black_list.end ())
    {
        black_list.insert (state);
        free_nfa (state->out1_);
        free_nfa (state->out2_);
        delete state;
    }
}

fragment_t *nfa_t::create_atom (state_t *state)
{
    return new fragment_t (std::list<state_t **> (1, &state->out1_), state);
}

fragment_t *nfa_t::create_kleenestar (fragment_t *f)
{
    state_t *epsilon = new state_t (state_t::Split, f->start_, NULL);
    std::list<state_t **> output (1, &epsilon->out2_);
    std::list<state_t **>::iterator it = f->out_.begin ();

    for (; it != f->out_.end (); it++)
    {
        **it = epsilon;
    }

    return new fragment_t (output, epsilon);
}

fragment_t *nfa_t::create_concatenation (fragment_t *f1, fragment_t *f2)
{
    std::list<state_t **>::iterator it = f1->out_.begin ();

    for (; it != f1->out_.end (); it++)
    {
        **it = f2->start_;
    }

    return new fragment_t (f2->out_, f1->start_);
}

fragment_t *nfa_t::create_alternation (fragment_t *f1, fragment_t *f2)
{
    std::copy 
    (
        f2->out_.begin (), 
        f2->out_.end (), 
        std::back_inserter (f1->out_)
    );
    
    state_t *epsilon = new state_t 
    (
        state_t::Split, 
        f1->start_, 
        f2->start_
    );
    
    return new fragment_t (f1->out_, epsilon);
}

void nfa_t::add_state (std::list<state_t *> &id_list, state_t *state)
{
    if (state == NULL || state->id_ == id_counter_)
    {
        return;
    }

    state->id_ = id_counter_;
    
    if (state->get_char () == state_t::Split)
    {
        /* follow unlabeled arrows (copypasted comment) */

        add_state (id_list, state->out1_);
        add_state (id_list, state->out2_);
        return;
    }

    id_list.push_back (state);
}


