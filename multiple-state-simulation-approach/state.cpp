



#include "state.h"

state_t::state_t (int c, state_t *out1, state_t *out2)

    :out1_ (out1)
    ,out2_ (out2)
    ,id_   (-1)
    ,char_ (c)
{
    /* void */
}

int state_t::get_char () const
{
    return char_;
}


