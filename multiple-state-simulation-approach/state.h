



#ifndef _REGEX_COMPILER_STATE_H
#define _REGEX_COMPILER_STATE_H


/* The state of NFA.
 * The state constructs from label of incoming arc and 
 * two outgoing arcs.
 */

class state_t
{
    public:

        enum
        {
            Final = 256,  // It's a final state.
            Split = 257   // Both outgoing states are valid.
        };

        state_t (int character, state_t *out1, state_t *out2);
        
        int get_char () const;

        state_t *out1_;  // first outgoing arrow
        state_t *out2_;  // second outgoing arrow 
        int id_;         // required for nfa's execution

    private:

        int char_; 
};

#endif  // _REGEX_COMPILER_STATE_H




