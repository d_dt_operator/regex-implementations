



#ifndef _REGEX_COMPILER_MATCH_H
#define _REGEX_COMPILER_MATCH_H

#include <string>


/* Test function.
 * If regex matches input, then true is returned,
 * otherwise false is returned.
 */

extern bool match (const std::string &regex, const std::string &input);

#endif  // _REGEX_COMPILER_MATCH_H



