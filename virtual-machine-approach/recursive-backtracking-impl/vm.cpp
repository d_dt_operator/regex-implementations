
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include "vm-description/vm_instructions.h"
#include "recursive-backtracking-impl/vm.h"

vm_t::vm_t (std::list<vm_instruction_t *> &bytecode)

    :bytecode_ (bytecode)
{
    /* void */
}

static void clean (vm_instruction_t *arg) {delete arg;};

vm_t::~vm_t ()
{
    std::for_each
    (
        bytecode_.begin (),
        bytecode_.end (),
        clean
    );
}

bool vm_t::match (const std::string &input)
{
    input_ = input;
    return run (*bytecode_.begin (), input_.begin ());
}

bool vm_t::run (pc_t pc, sp_t sp)
{
    assert (pc != NULL);

    if (pc->code_ == vm_code_char) 
        return (sp != input_.end ()) && (pc->char_ == *sp) && run (pc->next_, sp + 1);
    else if (pc->code_ == vm_code_jmp) 
        return run (pc->branch1_, sp);
    else if (pc->code_ == vm_code_split)
        return run (pc->branch1_, sp) || run (pc->branch2_, sp);
    else if (pc->code_ == vm_code_eps) 
        return run (pc->next_, sp);
    else if (pc->code_ == vm_code_match) 
        return sp == input_.end ();

    assert (0);
    return false;
}



