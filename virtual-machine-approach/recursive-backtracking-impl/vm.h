
#ifndef RECURCIVE_BACKTRACKING_IMPLEMENTATION_VM_H
#define RECURCIVE_BACKTRACKING_IMPLEMENTATION_VM_H

#include <string>
#include <list>

struct vm_instruction_t;

class vm_t
{
    public:

        /* Machine takes ownership on the set of instructions.
         */
        vm_t (std::list<vm_instruction_t *> &bytecode);
       ~vm_t ();

        bool match (const std::string &input);

    private:

        typedef std::string::const_iterator sp_t;
        typedef vm_instruction_t *pc_t;

        bool run (pc_t, sp_t);

        std::string input_;
        std::list<vm_instruction_t *> bytecode_;
};

#endif  // RECURCIVE_BACKTRACKING_IMPLEMENTATION_VM_H




