
#include <stdio.h>
#include <algorithm>
#include "recursive-backtracking-impl/vm.h"
#include "vm-description/vm_instructions.h"
#include "bytecode-compiler/compiler.h"
#include "bytecode-compiler/debug.h"

static void clean (vm_instruction_t *arg) {delete arg;}

int main (int argc, char *argv[])
{
    if (argc != 3)
    {
        fprintf (stderr, "\n main: unexpected amount of arguments.\n");
        fprintf (stderr, " main: try %s {regex} {input}.\n\n", argv[0]);
        return 1;
    }

    std::list<vm_instruction_t *> bytecode;
    compile (argv[1], bytecode);
    print_instructions (bytecode);
    vm_t vm (bytecode);

    printf ("main: regex = %s\n", argv[1]);
    printf ("main: input = %s\n", argv[2]);
    printf ("main: match = %d\n", vm.match (argv[2]));
    return 0;
}

