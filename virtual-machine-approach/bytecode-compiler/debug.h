



#ifndef VIRTUAL_MACHINE_APPROACH_BYTECODE_COMPILER_DEBUG_H
#define VIRTUAL_MACHINE_APPROACH_BYTECODE_COMPILER_DEBUG_H

#include <list>

struct vm_instruction_t;

/* Print bytecode instructions.
 * This function exists only for debugging purpose.
 * The function checks correctness of the instructions set,
 * using assert macro. So if there is an error in the 
 * instruction, then program will be terminated.
 */
extern void print_instructions (const std::list<vm_instruction_t *> &);

#endif  // VIRTUAL_MACHINE_APPROACH_BYTECODE_COMPILER_DEBUG_H




