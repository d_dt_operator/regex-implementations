



#include <assert.h>
#include <stdio.h>
#include <string>
#include <list>
#include <algorithm>
#include "vm-description/vm_instructions.h"
#include "bytecode-compiler/compiler.h"
#include "bytecode-compiler/parser.h"

static bool is_terminal (char arg)
{
    return arg != '(' && arg != ')' && arg != '+' && arg != '*';
}

static bool is_concatenation (char op1, char op2)
{
    if (is_terminal (op1) && is_terminal (op2))
        return true;
    if (is_terminal (op1) && op2 == '(')
        return true;
    if (op1 == '*' && is_terminal (op2))
        return true;
    if (op1 == ')' && is_terminal (op2))
        return true;
    if (op1 == '*' && op2 == '(')
        return true;
    
    return false;
}

static bool add_conc_operator (const std::string &input, std::string &out)
{
    /* Check correctenss of input. */
    assert (input.find ('.') == std::string::npos);
    assert (! input.empty ());

    std::string::const_iterator curr = input.begin ();
    std::string::const_iterator next = curr + 1;

    for (; next != input.end (); curr++, next++)
    {
        out.push_back (*curr);
        if (is_concatenation (*curr, *next))
            out.push_back ('.');
    }

    out.push_back (*curr);
    return true;
}

void compile (const std::string &regex, std::list<vm_instruction_t *> &bytecode)
{
    /* Parse regex. */
    std::string input;
    add_conc_operator (regex, input);
    regex_parser_t ().parse (input, bytecode);
    
    /* Add match instruction. */
    vm_instruction_t *match = new vm_instruction_t ();
    match->code_ = vm_code_match;
    bytecode.push_back (match);
    
    /* Initialize next-instruction links. */
    std::list<vm_instruction_t *>::iterator it0 = bytecode.begin ();
    std::list<vm_instruction_t *>::iterator it1 = it0;

    for (; it0 != bytecode.end (); ++it0)
    {
        if (++it1 == bytecode.end ())
            break;

        (*it0)->next_ = *it1;
    }
}


