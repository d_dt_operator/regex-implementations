



#include <stdio.h>
#include <assert.h>
#include <map>
#include <numeric>
#include <algorithm>
#include "bytecode-compiler/debug.h"
#include "vm-description/vm_instructions.h"

typedef std::map<const vm_instruction_t *, int> labels_t;

struct labels_func_t
{
    labels_t &operator () (labels_t &, const vm_instruction_t *) const;
};

labels_t &labels_func_t::operator () (labels_t &arg1, const vm_instruction_t *arg2) const
{
    if (arg2->branch1_ != NULL)
        arg1.insert (std::make_pair (arg2->branch1_, arg1.size ()));
    if (arg2->branch2_ != NULL)
        arg1.insert (std::make_pair (arg2->branch2_, arg1.size ()));
    return arg1;
};

struct print_func_t
{
    print_func_t (const labels_t &l):labels_ (l) {}
    void operator () (const vm_instruction_t *) const;
    const labels_t &labels_;
};

void print_func_t::operator () (const vm_instruction_t *arg) const
{
    int l1 = -1, l2 = -1, l3 = -1;
    labels_t::const_iterator it1 = labels_.find (arg->branch1_);
    labels_t::const_iterator it2 = labels_.find (arg->branch2_);
    labels_t::const_iterator it0 = labels_.find (arg);
    vm_codes_t code = arg->code_;

    assert (! (code == vm_code_split && it1 == labels_.end ()));
    assert (! (code == vm_code_split && it2 == labels_.end ()));
    assert (! (code == vm_code_jmp   && it1 == labels_.end ()));

    if (code == vm_code_split || code == vm_code_jmp)
        l1 = it1->second;
    if (code == vm_code_split)
        l2 = it2->second;
    if (it0 != labels_.end ())
        l3 = it0->second;

    char prefix[] = "    ";

    if (l3 >= 0)
        sprintf (prefix, "L%02d:", l3);

    if (code == vm_code_char)  
        printf ("%s char \'%c\'\n", prefix, arg->char_);
    else if (code == vm_code_match) 
        printf ("%s match\n", prefix);
    else if (code == vm_code_jmp)   
        printf ("%s jmp L%d\n", prefix, l1);
    else if (code == vm_code_split) 
        printf ("%s split L%d, L%d\n", prefix, l1, l2);
    else if (code == vm_code_eps)   
        printf ("%s eps\n", prefix);
    else
        printf ("unknown-code\n");
}

void print_instructions (const std::list<vm_instruction_t *> &arg)
{
    labels_func_t labels_func;
    labels_t labels;
    labels = std::accumulate (arg.begin (), arg.end (), labels_t (), labels_func);
    std::for_each (arg.begin (), arg.end (), print_func_t (labels));
}


