



#ifndef VIRTUAL_MACHINE_APPROACH_BYTECODE_COMPILER_PARSER_H
#define VIRTUAL_MACHINE_APPROACH_BYTECODE_COMPILER_PARSER_H

/* 
 * Simple LL(1) parser realization for CFG, describing 
 * simple regex syntax:
 * 
 * s  ->  ba'      
 * a' -> +ba'|eps 
 * b  ->  cb'      
 * b' -> .cb'|eps 
 * c  ->  dc'      
 * c' -> *|eps    
 * d  -> (s)|char 
 */

#include <list>
#include <string>

class regex_parser_t
{
    public:

        regex_parser_t ();
       ~regex_parser_t ();

        /* Parse regex, convert it to bytecode and
         * write result bytecode into second argument.
         * If there are no errors occur, then return true.
         * Otherwise return false.
         */
        bool parse (const std::string &regex, std::list<vm_instruction_t *> &);

    private:

        typedef std::list<vm_instruction_t *> fragment_t;

        /* Methods for each of non-terminals of CFG. */
        void process_s  ();
        void process_a_ ();
        void process_b  ();
        void process_b_ ();
        void process_c  ();
        void process_c_ ();
        void process_d  ();

        /* Helpers.
         * creat_concat - Create concatenation of bytecode fragments op1 and op2, write result to dst.
         * creat_altern - Create alternative from bytecode fragments op1 and op2, write result to dst.
         * creat_kleene - Create Kleene star for bytecode fragment op, write result to dst.
         * join_fragmts - Join bytecode fragments dst and src, write result to dst.
         * consume_char - Increment input string iterator.
         * look_ahead   - Get current input string symbol.
         */
        void creat_concat (fragment_t &dst, fragment_t &op1, fragment_t &op2);
        void creat_altern (fragment_t &dst, fragment_t &op1, fragment_t &op2);
        void creat_kleene (fragment_t &dst, fragment_t &op);
        void join_fragmts (fragment_t &dst, fragment_t &src);
        void consume_char ();
        char look_ahead   ();

        /* Parser's state */
        std::string input_;
        std::string::const_iterator it_;
        std::list<fragment_t> stack_;
};

#endif  // VIRTUAL_MACHINE_APPROACH_BYTECODE_COMPILER_PARSER_H


