



#ifndef VIRTUAL_MACHINE_APPROACH_BYTECODE_COMPILER_COMPILER_H
#define VIRTUAL_MACHINE_APPROACH_BYTECODE_COMPILER_COMPILER_H

#include <list>
#include <string>

struct vm_instruction_t;

/* Complile regular expression to virtual machine bytecode.
 * Return value is the list of virtual machine's instructions.
 * If compilation error occur, then empty list will be returned.
 */
extern void compile (const std::string &regex, std::list<vm_instruction_t *> &bytecode);

#endif  // VIRTUAL_MACHINE_APPROACH_BYTECODE_COMPILER_COMPILER_H




