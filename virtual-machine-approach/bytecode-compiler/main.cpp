



#include <string.h>
#include <stdio.h>
#include <string>
#include <list>
#include <map>
#include <algorithm>
#include "vm-description/vm_instructions.h"
#include "bytecode-compiler/compiler.h"
#include "bytecode-compiler/debug.h"

static void clean (vm_instruction_t *arg) {delete arg;}

int main (int argc, char *argv[])
{
    if (argc != 2)
    {
        fprintf (stderr, "\n main: unexpected amount of arguments.\n");
        fprintf (stderr, " main: try %s {regex}.\n\n", argv[0]);
        return 1;
    }

    std::list<vm_instruction_t *> bytecode;
    compile (argv[1], bytecode);
    print_instructions (bytecode);
    std::for_each (bytecode.begin (), bytecode.end (), clean);
    return 0;
}



