



#include <assert.h>
#include <stdio.h>
#include <list>
#include <algorithm>
#include "vm-description/vm_instructions.h"
#include "bytecode-compiler/parser.h"

regex_parser_t::regex_parser_t ()

    :it_ (input_.end ())
{
    /* void */
}

static void clean_instruction (vm_instruction_t *arg)
{
    delete arg;
}

static void clean_stack (std::list<vm_instruction_t *> &arg)
{
    std::for_each (arg.begin (), arg.end (), clean_instruction);
}

regex_parser_t::~regex_parser_t ()
{
    std::for_each (stack_.begin (), stack_.end (), clean_stack);
}

bool regex_parser_t::parse (const std::string &regex, std::list<vm_instruction_t *> &bytecode)
{
    input_ = regex;
    it_ = input_.begin ();
    process_s ();

    /* check on correctness of parsing. */
    assert (stack_.size () == 1);
    
    bytecode = stack_.back ();
    stack_.clear ();
    return true;
}

void regex_parser_t::process_s ()
{
    /* a --> ba' */
    process_b  (); 
    process_a_ ();
}

void regex_parser_t::process_a_ ()
{
    char ch = look_ahead ();

    /* a' --> eps */
    if (ch != '+')
        return;

    /* a' --> +ba' */
    consume_char ();
    process_b    ();
    process_a_   ();

    fragment_t op2 = stack_.back ();
    stack_.pop_back ();
    fragment_t op1 = stack_.back ();
    stack_.pop_back ();
    fragment_t dst;
    creat_altern (dst, op1, op2);
    stack_.push_back (dst);
}

void regex_parser_t::process_b ()
{
    /* b --> cb' */
    process_c  ();
    process_b_ ();
}

void regex_parser_t::process_b_ ()
{
    char ch = look_ahead ();

    /* b' --> eps */    
    if (ch != '.')
        return;

    /* b' --> .cb' */
    consume_char ();
    process_c    ();
    process_b_   ();

    fragment_t op2 = stack_.back (); 
    stack_.pop_back ();
    fragment_t op1 = stack_.back (); 
    stack_.pop_back ();
    fragment_t dst;
    creat_concat (dst, op1, op2);
    stack_.push_back (dst);
}

void regex_parser_t::process_c ()
{
    /* c --> dc' */
    process_d  ();
    process_c_ ();
}

void regex_parser_t::process_c_ ()
{
    char ch = look_ahead ();

    /* c' --> eps */
    if (ch != '*')
        return;

    /* c' --> * */
    consume_char ();
    fragment_t op = stack_.back ();
    stack_.pop_back ();
    fragment_t dst;
    creat_kleene (dst, op);
    stack_.push_back (dst);
}

void regex_parser_t::process_d ()
{
    char ch = look_ahead ();

    if (ch == '(')
    {
        /* d --> (a) */
        consume_char ();
        process_s ();
        ch = look_ahead ();
        
        if (ch != ')')
        {
            fprintf (stderr, "d: mismatch in parenthesis was detected");
            fprintf (stderr, "d: character == %c\n", ch);
            fprintf (stderr, "d: parsing was aborted\n");
            return;
        }

        consume_char ();
        return;
    }
    
    /* d --> char */
    consume_char ();
    
    /* create vm instruction */
    vm_instruction_t *cmd = new vm_instruction_t ();
    cmd->code_ = vm_code_char;
    cmd->char_ = ch;

    /* create bytecode fragment */
    fragment_t fragment;
    fragment.push_back (cmd);
    stack_.push_back (fragment);
}

void regex_parser_t::creat_concat (fragment_t &dst, fragment_t &op1, fragment_t &op2)
{
    dst.clear ();
    join_fragmts (dst, op1);
    join_fragmts (dst, op2);
}

void regex_parser_t::creat_altern (fragment_t &dst, fragment_t &op1, fragment_t &op2)
{
    vm_instruction_t *cmd_spt = new vm_instruction_t ();
    vm_instruction_t *cmd_jmp = new vm_instruction_t ();
    vm_instruction_t *cmd_eps = new vm_instruction_t ();

    /* Initialize vm instructions. */
    cmd_spt->code_ = vm_code_split;
    cmd_jmp->code_ = vm_code_jmp;
    cmd_eps->code_ = vm_code_eps;
    cmd_spt->branch1_ = op1.front ();
    cmd_spt->branch2_ = op2.front ();
    cmd_jmp->branch1_ = cmd_eps;
        
    /* Fill dst fragment. */
    dst.push_back (cmd_spt);
    join_fragmts (dst, op1);
    dst.push_back (cmd_jmp);
    join_fragmts (dst, op2);
    dst.push_back (cmd_eps);
}

void regex_parser_t::creat_kleene (fragment_t &dst, fragment_t &op)
{
    vm_instruction_t *cmd_spt = new vm_instruction_t ();
    vm_instruction_t *cmd_jmp = new vm_instruction_t ();
    vm_instruction_t *cmd_eps = new vm_instruction_t ();
    
    /* Init instructions and set labels. */
    cmd_spt->code_ = vm_code_split;
    cmd_jmp->code_ = vm_code_jmp;
    cmd_eps->code_ = vm_code_eps;
    cmd_spt->branch1_ = op.front ();
    cmd_spt->branch2_ = cmd_eps;
    cmd_jmp->branch1_ = cmd_spt;
    
    /* Fill dst fragment. */
    dst.push_back (cmd_spt);
    join_fragmts (dst, op);
    dst.push_back (cmd_jmp);
    dst.push_back (cmd_eps);
}


void regex_parser_t::join_fragmts (fragment_t &dst, fragment_t &src)
{
    std::copy (src.begin (), src.end (), std::back_inserter (dst));
}

void regex_parser_t::consume_char ()
{
    if (it_ == input_.end ())
        return;

    it_++;
}

char regex_parser_t::look_ahead ()
{
    if (it_ == input_.end ())
        return '\0';

    return *it_;
}


