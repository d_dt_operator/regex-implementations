



#ifndef RECURCIVE_BACKTRACKING_IMPLEMENTATION_VM_INSTRUCTIONS_H
#define RECURCIVE_BACKTRACKING_IMPLEMENTATION_VM_INSTRUCTIONS_H

/* Virtual machine instructions codes. 
 */
enum vm_codes_t
{
    vm_code_char,
    vm_code_jmp,
    vm_code_split,
    vm_code_eps,
    vm_code_match
};

/* Virtual machine instruction.
 * branch[12]_ is possible branches (in case of split & jmp instructions).
 * char_ is instruction character (in case of char instruction).
 * code_ is instruction code.
 */
struct vm_instruction_t
{
    vm_instruction_t ();
    vm_instruction_t *branch1_;
    vm_instruction_t *branch2_;
    vm_instruction_t *next_;
    vm_codes_t code_;
    char char_;
};

#endif  // RECURCIVE_BACKTRACKING_IMPLEMENTATION_VM_INSTRUCTIONS_H




